using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Utils;

public class ExtensionsTests
{
    [TestCase(1, 2, 0, 10, 0, 20)]
    [TestCase(2, 1, 0, 20, 0, 10)]
    [TestCase(1, 9, 0, 10, 10, 0)]
    public void FloatMapTest(float value, float expectedValue, float fromSource, float toSource, float fromTarget, float toTarget)
    {
        value = value.Map(fromSource, toSource, fromTarget, toTarget);
        Assert.AreEqual(expectedValue, value);
    }
}
