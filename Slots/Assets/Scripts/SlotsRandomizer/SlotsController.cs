﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Slots.UI;

namespace Slots.Randomizer
{
    public class SlotsController : MonoBehaviour
    {
        [SerializeField]
        private string itemsPath = "Items/";
        [SerializeField]
        private RowView[] rowViews;

        private SlotItem[] avaliableSlotItems;
        private List<Row> rows = new List<Row>();


        private void Awake()
        {
            SetupAvaliableSlotItems();
            SetupRows();
        }

        private void SetupRows()
        {
            for (int x = 0; x < rowViews.Length; x++)
            {
                Row row = new Row(rowViews[x]);
                rows.Add(row);
                for (int y = 0; y < rowViews[x].slotsCount; y++)
                {
                    Slot slot = new Slot();
                    slot.item = GetRandomItem();
                    row.AddSlot(slot);
                    rowViews[x].SlotViews[y].SetData(slot.item.Sprite);
                }
            }
        }

        private void SetupAvaliableSlotItems()
        {
            avaliableSlotItems = Resources.LoadAll<SlotItem>(itemsPath);
            avaliableSlotItems = avaliableSlotItems.OrderBy(s => s.GetWeight()).ToArray();
        }

        private SlotItem GetRandomItem()
        {
            float rand = Random.value;
            for (int i = avaliableSlotItems.Length - 1; i >= 0; i--)
            {
                if(rand >= avaliableSlotItems[i].GetWeight())
                {
                    return avaliableSlotItems[i];
                }
            }
            return avaliableSlotItems[0];
        }
    }
}
