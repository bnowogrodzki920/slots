﻿using Slots.UI;
using System.Collections.Generic;

namespace Slots.Randomizer
{
    public class Row
    {
        private List<Slot> slots = new List<Slot>();
        private RowView rowView;

        public Row(RowView rowView)
        {
            this.rowView = rowView;
        }

        public void AddSlot(Slot slot)
        {
            slots.Add(slot);
        }

        public void RemoveSlot(Slot slot)
        {
            slots.Remove(slot);
        }

        public bool ContainsSlot(Slot slot)
        {
            return slots.Contains(slot);
        }
    }
}