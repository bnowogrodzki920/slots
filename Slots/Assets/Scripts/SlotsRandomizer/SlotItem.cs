using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Slots.Randomizer
{
    [CreateAssetMenu(fileName = "New Slot Item", menuName = "Slots/New Slot Item")]
    public class SlotItem : ScriptableObject
    {
        [field: SerializeField]
        public string Name { get; private set; } = "New Slot Item";
        [field: SerializeField]
        public Sprite Sprite;
        [field: Range(0.25f, 5), SerializeField]
        public float StakeMultiplier { get; private set; } = 2;

        public float GetWeight()
        {
            return StakeMultiplier.Map(0.25f, 5, 0.01f, 1);
        }
    } 
}