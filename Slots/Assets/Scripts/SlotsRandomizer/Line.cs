﻿using UnityEngine;
using Slots.UI;

namespace Slots.Randomizer
{
    [System.Serializable]
    public struct Line
    {
        public Line(SlotView slotView)
        {
            SlotView = slotView;
        }

        [field: SerializeField]
        public SlotView SlotView { get; private set; }
    }
}
