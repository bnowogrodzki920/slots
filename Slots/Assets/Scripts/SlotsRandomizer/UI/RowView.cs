﻿using DG.Tweening;
using UnityEngine;
using Utils;

namespace Slots.UI
{
    public class RowView : View
    {
        [field: SerializeField]
        public SlotView[] SlotViews { get; private set; }
        public int slotsCount => SlotViews.Length;
        [SerializeField]
        private float YposToLerp;
        [SerializeField]
        private Ease ease;
        private RectTransform rectTransform => transform as RectTransform;

        [ContextMenu("SlideDown")]
        public async void SlideDown()
        {
            float YstartPos = rectTransform.anchoredPosition.y;
            Sequence sequence = DOTween.Sequence();
            sequence.Join(rectTransform.DOLocalMoveY(YposToLerp, Utilities.TWEEN_DURATION).SetEase(ease));
            await sequence.Play().AsyncWaitForCompletion();
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, YstartPos);
            SwapSlotViews();
        }

        private void SwapSlotViews()
        {
            for (int i = 0; i < 5; i++) 
            { 
                Transform child = transform.GetChild(0);
                child.SetAsLastSibling();
            }
        }
    }
}