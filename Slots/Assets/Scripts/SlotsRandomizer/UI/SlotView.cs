﻿using UnityEngine;
using UnityEngine.UI;

namespace Slots.UI
{
    public class SlotView : View
    {
        [SerializeField]
        private Image image;

        public void SetData(Sprite icon)
        {
            image.sprite = icon;
        }
    }
}